# Table Parser for simple definitions #

This table parser which parse excel files is to build simple definition tables. This parser generates only json and C# enums and classes until current version.

### How do I get set up? ###

Just need python3 and xlrd which can install from pip to run script. To test if it works properly, you can run compile.bat on windows or compile.sh on mac.

### How to use it ###

Tables in excel should follow some rules. To check the rule, you just take a look in res/Sample.xlsx and res/RpgDefines.xlsx. Every table should have with !TABLE, !FIELD_NAME, !FIELD_TYPE and !EOF in cols and !EOT in rows. Coloring red is must-field, you can check res/RpgDefines.xlsx. Columns should have type which can be 

* int
* float
* bool
* string - string type is 32 bytes as default. But you can assign string64, string128, string256 if needed.
* custom type (ex. !ITEM_TYPE, !HERO_JOB_TYPE)

Custom type can be used in other excel files.

All excel files should locate in /res/*.xlsx. Parser try to compile all excels in /res. then produce all keys and tables in json and source codes in C#(C++ too). 

Every key-value is kept in **enum_db.json**. Value of key is assigned in order. For example, in res/RpgDefines.xlsx, you can see first record in **key** field that ACTION_CREATE_FOX assigned 1. Then following records are assigned 2, 3, 4,... automatically. So if you delete enum_db.json by mistake, you don't need to worry to recover all key values. However I recommed to keep it in your project repository for sure.

### After compiling ###

You can result ./json-gen, ./auto-gen, ./enum-gen and ./hpp-gen. When error occured result.txt will show you what's the problem.


