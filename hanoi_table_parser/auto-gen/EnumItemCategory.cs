// TableTool auto-generated Table class: EnumItemCategory at 2019.07.05, 14:40:01
// by jazzcake@gmail.com

using System.Collections.Generic;
using System;

namespace GameTable {
	public class EnumItemCategory : IGameTable {
		public ITEM_TYPE key;
		public int arg_int0;
		public string arg_str0;
		public SAMPLE_TYPE arg_enum0;

		// if load table, this dictionary has all records from json file.
		public static Dictionary<ITEM_TYPE, EnumItemCategory> table_dic = null;
		
		// create instance based on json file.
		public static EnumItemCategory Create(Dictionary<string, object> json_chunk)
		{
			return new EnumItemCategory()
			{
				key = (ITEM_TYPE)((long)json_chunk["key"]),
				arg_int0 = (int)((long)json_chunk["arg_int0"]),
				arg_str0 = json_chunk["arg_str0"] as string,
				arg_enum0 = (SAMPLE_TYPE)((long)json_chunk["arg_enum0"]),

			};
		}
		
		#region [Methods for tools]
		public static bool Load()
		{
			if (table_dic == null)
				table_dic = LoadTable<ITEM_TYPE, EnumItemCategory>(GetFilename(typeof(EnumItemCategory)));

			return (table_dic != null);
		}

		public static List<string> GetAllKeys()
		{
			var all_keys = new List<string>();
			foreach (var key in table_dic.Keys)
				all_keys.Add(key.ToString());
			return all_keys;
		}

		public static bool HasKey(string key_string)
		{
			ITEM_TYPE value;
			return Enum.TryParse<ITEM_TYPE>(key_string, out value);
		}

		public static ITEM_TYPE ConvertKey(string key_string)
		{
			ITEM_TYPE value;
			if (Enum.TryParse<ITEM_TYPE>(key_string, out value))
				return value;

			return (ITEM_TYPE)int.MinValue;
		}

		public static EnumItemCategory GetValue(string key_string)
		{
			var key = ConvertKey(key_string);
			if (table_dic.ContainsKey(key))
				return table_dic[key];
			return null;
		}
		#endregion
	};
}