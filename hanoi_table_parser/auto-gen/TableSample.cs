// TableTool auto-generated Table class: TableSample at 2019.07.05, 14:40:01
// by jazzcake@gmail.com

using System.Collections.Generic;
using System;

namespace GameTable {
	public class TableSample : IGameTable {
		public SAMPLE_TYPE key;
		public string string_value;
		public int int_value;
		public float float_value;
		public bool bool_value;

		// if load table, this dictionary has all records from json file.
		public static Dictionary<SAMPLE_TYPE, TableSample> table_dic = null;
		
		// create instance based on json file.
		public static TableSample Create(Dictionary<string, object> json_chunk)
		{
			return new TableSample()
			{
				key = (SAMPLE_TYPE)((long)json_chunk["key"]),
				string_value = json_chunk["string_value"] as string,
				int_value = (int)((long)json_chunk["int_value"]),
				float_value = (float)((double)json_chunk["float_value"]),
				bool_value = ((long)json_chunk["bool_value"] != 0),

			};
		}
		
		#region [Methods for tools]
		public static bool Load()
		{
			if (table_dic == null)
				table_dic = LoadTable<SAMPLE_TYPE, TableSample>(GetFilename(typeof(TableSample)));

			return (table_dic != null);
		}

		public static List<string> GetAllKeys()
		{
			var all_keys = new List<string>();
			foreach (var key in table_dic.Keys)
				all_keys.Add(key.ToString());
			return all_keys;
		}

		public static bool HasKey(string key_string)
		{
			SAMPLE_TYPE value;
			return Enum.TryParse<SAMPLE_TYPE>(key_string, out value);
		}

		public static SAMPLE_TYPE ConvertKey(string key_string)
		{
			SAMPLE_TYPE value;
			if (Enum.TryParse<SAMPLE_TYPE>(key_string, out value))
				return value;

			return (SAMPLE_TYPE)int.MinValue;
		}

		public static TableSample GetValue(string key_string)
		{
			var key = ConvertKey(key_string);
			if (table_dic.ContainsKey(key))
				return table_dic[key];
			return null;
		}
		#endregion
	};
}