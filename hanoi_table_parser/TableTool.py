﻿# from __future__ import print_function # must be top of module in case of python 2x
import subprocess

def install(name):
    subprocess.call(['pip', 'install', name])

# install necessaries
install('xlrd')

from xlrd import open_workbook
import os
import re
import operator
import codecs
import sys
import json
import datetime

# global error displayer
g_error_displayer = 0			# 0: default, 1: show field, 2: show perfect log

# global vars
g_table_infos = [] # keep format of each table
g_table_contents = {}
g_table_enums = {} # keep key id for whole tables
g_error_comment = []

# global consts
g_reg_def = r'(\d+|\w+)'

# global methods

# check type of !TYPEXXX in excel sheet.
def IsKeywordType(x):
	return (x == '!INT' or x == '!LONG' or x == '!FLOAT' or x == '!BOOL' or x == '!STRING' or x == '!STRING32' or x == '!STRING64' or x == '!STRING128' or x == '!STRING256' or x == '!STRING512' or x == '!STRING1024' or x == '!STRINGX')
		
# check type of !USERTYPEXXX in excel sheet.
def IsEnumType(x):
	if (type(x) is str):
		b = [i for i in re.split(g_reg_def, x) if i]
		return (len(b) == 2 and b[0] == '!' and type(b[1]) == str)
		
	return False
	
# check if enum define?
def IsEnumTypeDefine(x):
	if (type(x) is str):
		b = [i for i in re.split(g_reg_def, x) if i]
		return (len(b) == 2 and b[0] == '$' and type(b[1]) == str)
		
	return False
	
# get enum type name from define
def ExtractEnumTypeName(x):
	if (type(x) is str):
		b = [i for i in re.split(g_reg_def, x) if i]
		if (len(b) == 2 and (b[0] == '$' or b[0] == '!') and type(b[1]) == str):
			return b[1]
			
	return ""
	
# get enum name and value
def ExtractEnumNameAndValue(x, value):
	if (type(x) is str):
		x = str(x).replace(' ', '')
		b = [i for i in re.split(g_reg_def, x) if i]

		if (len(b) == 3 and b[1] == '=' and type(b[0]) == str and (type(b[2]) == str or type(b[2]) == int)):
			return [b[0], int(b[2])]
		if (len(b) == 1 and type(b[0]) == str):
			return [b[0], int(value)]

	return []
	
# update enum and value
def UpdateEnumAndValue(type_name, enum_name, enum_value):
	if type_name in g_table_enums:
		enum_dic = g_table_enums[type_name]
		
		# check if formal value must be same with new enum_value
		if enum_name in enum_dic:
			if enum_dic[enum_name] != enum_value:
				return False
				
		enum_dic[enum_name] = enum_value
	else:
		enum_dic = {}
		enum_dic[enum_name] = enum_value
		g_table_enums[type_name] = enum_dic
		
	return True

# get enum value	
def GetEnumAsInt(type_name, enum_name):
	enum_dic = g_table_enums[type_name]
	return enum_dic[enum_name]

# is exist enum
def IsEnumExist(type_name, enum_name):
	if type_name in g_table_enums:
		enum_dic = g_table_enums[type_name]
		if enum_name in enum_dic:
			return True
	
	return False

#----------------------------------------------------------------------------------------------
# table parser class
class ExcelTableParser:
	_sheet_name = 'noname for sheet'
	_sheet_obj = None

	def __init__(self, sheet_obj):
		self._sheet_obj = sheet_obj

		print('	- max col ', sheet_obj.ncols, ' max row ', sheet_obj.nrows)

	def validate_cell(self, row, col, cell_type, cell_value):
		return (self._sheet_obj.cell_type(row, col) == cell_type and self._sheet_obj.cell_value(row, col) == cell_value)

	def read_table(self, row, col):
		# parameter: cell(row, col) MUST is !TABLE

		global g_error_comment

		# get table name
		table_name = self._sheet_obj.cell_value(row, col + 1)

		# collect all 'field name'
		base_row = row + 1
		base_col = col + 1
		field_names = self._sheet_obj.row_values(base_row, base_col, self._sheet_obj.ncols)
		field_types = self._sheet_obj.row_values(base_row + 1, base_col, self._sheet_obj.ncols)

		# key must be name of first col.
		if (field_names[0] != 'key'):
			g_error_comment.append('### Error# First col must have "key" as field_name but ' + field_names[0] + ' in ' + table_name)
			field_names[0] = 'key'

		# find !EOF
		index_eof = field_names.index('!EOF') + base_col

		# find !EOT
		row_types = self._sheet_obj.col_values(col, base_row, self._sheet_obj.nrows)
		index_eot = row_types.index('!EOT') + base_row

		print('	- Table Found:', table_name, ' EOF:', index_eof, ' EOT:', index_eot)

		# record format of table
		g_table_infos.append([table_name, base_row, base_col, field_names, field_types, index_eof, index_eot])

		# collect contents of table
		contents = []
		# adjust base_row for contents
		base_row = row + 3

		# collect enums
		for field_index in range(0, index_eof - base_col):
			field_type = field_types[field_index]
			
			# find "enum" that is enum which start with '$'.
			if (IsEnumTypeDefine(field_type)):
				enum_type = ExtractEnumTypeName(field_type)
				enum_inc_value = -1 # enum start 1 as default
				
				# collect all enum value.
				enum_values = self._sheet_obj.col_values(base_col + field_index, base_row, index_eot)

				for v in enum_values:
					if type(v) != str or len(v) <= 0 or v[0] == '#':
						continue

					enum_info = ExtractEnumNameAndValue(v, enum_inc_value)
					
					if len(enum_info) < 2:
						g_error_comment.append('### Error: has some problem in ' + v)
						return

					enum_name = enum_info[0]
					enum_value = enum_info[1]
					
					# enum_value must > 0
					if (enum_value < 0):
						g_error_comment.append('### Error: Enum({0}) value must be assigned with >=0. Parsing this excel terminated.'.format(enum_name))
						return
						
					# print('EnumValue: {0} - {1}'.format(enum_info[0], enum_info[1]))
					if (UpdateEnumAndValue(enum_type, enum_name, int(enum_value)) == False):
						error_str = '### Error: {0}={1} MUST be consist. Check {2} enums in enum_db.json. Parsing this excel terminated'.format(enum_name, enum_value, enum_type)
						g_error_comment.append(error_str)
					
					# inc enum for the next
					enum_inc_value = enum_value + 1
					
		# collect content row by row
		for row_index in range(0, index_eot - base_row):
			row_contents = self._sheet_obj.row_values(row_index + base_row, base_col, index_eof)

			key = row_contents[0]

			if len(key) == 0:
				g_error_comment.append("### Error: Row {0} has empty contents in {1}".format(row_index, table_name))
				continue

			if key[0] == '#':
				print("	- Comment-out Line: ", row_index + base_row)
				continue

			contents.append(row_contents)
			
			# just show values in each row
			# for col_index in range(0, index_eof - base_col):
				# print('RowValue(', row_index, ') : ', self._sheet_obj.cell_value(base_row + row_index, base_col + col_index))


		g_table_contents[table_name] = contents

	def parse(self):
		global g_error_comment

		# cell type : http://www.lexicon.net/sjmachin/xlrd.html
		type_string = 1
		# type_float = 2
		# type_date = 3
		# type_bool = 4

		table_list = []
		
		print("\n> Try parsing:", self._sheet_obj.nrows, self._sheet_obj.ncols)

		for j in range(0, self._sheet_obj.nrows):
			for i in range(0, self._sheet_obj.ncols):

				# cell_type : 0:empty string, 1:unicode string, 2:float number, 3:date, 4:bolean, 5:error, 6:empty
				cell_type = self._sheet_obj.cell_type(j, i)
				cell_value = self._sheet_obj.cell_value(j, i)

				if (cell_type == type_string and cell_value == '!TABLE'):

					# verify & find end of table
					if (self.validate_cell(j + 1, i, type_string, '!FIELD_NAME') != True):
						g_error_comment.append('### Error:  Table ' + self._sheet_obj.name + ' has error on [' + (j+1) + ',' + i + ']')
					if (self.validate_cell(j + 2, i, type_string, '!FIELD_TYPE') != True):
						g_error_comment.append('### Error:  Table ' + self._sheet_obj.name + ' has error on [' + (j+1) + ',' + i, ']')

					table_list.append([j, i])

		for v in table_list:
			self.read_table(v[0], v[1])

# Parse single excel file
def ParseSingleExcelFile(filename):
	print('# Start parsing ', filename)
	
	book = open_workbook(filename, on_demand = True)

	print('# ', filename, ' has ', len(book.sheet_names()), ' sheets with it')

	for name in book.sheet_names():
		print('\n> Loading Sheet: ', name)

		# find sheet
		sheet = book.sheet_by_name(name)

		t = ExcelTableParser(sheet)
		t.parse()

		print("\n> Parse is done: ", name)

	print('\n# Finish parse\n')

# Replace IDs
def ReplaceIDs():
	global g_error_comment
	global g_error_displayer
	
	is_has_error = False

	for info in g_table_infos:
		table_name = info[0]
		# base_row = info[1]
		base_col = info[2]
		field_names = info[3]
		field_types = info[4]
		index_eof = info[5]
		# index_eot = info[6]

		print('> Validating IDs in ', table_name)
		
		is_has_id_field = False
		table_content = g_table_contents[table_name]

		for field_index in range(0, index_eof - base_col):
			field_type = field_types[field_index]
			field_name = field_names[field_index]

			if (g_error_displayer >= 1):
				print('	> Field type and name', field_type, field_name)
			
			if (IsKeywordType(field_type)):
				continue
				
			is_enum_def = IsEnumTypeDefine(field_type)
				
			# extract TYPENAME from '$TYPENAME' or '!TYPENAME'
			field_type = ExtractEnumTypeName(field_type)
			is_has_id_field = True
			
				
			for row_content in table_content:
				if ((row_content[field_index] == None or len(row_content[field_index]) == 0) and is_enum_def == False):
					continue
			
				enum_info = ExtractEnumNameAndValue(row_content[field_index], 0)
				enum_name = enum_info[0]
				# enum_value = enum_info[1]

				if enum_name == None or len(enum_name) == 0:
					continue
				
				if IsEnumExist(field_type, enum_name) == False: # not defined?
					is_has_error = True
					g_error_comment.append('### Error:  Unknown enum:<' + enum_name + '> in ' + table_name)
				else:
					row_content[field_index] = GetEnumAsInt(field_type, enum_name)

		if is_has_id_field == True:
			if is_has_error == True:
				g_error_comment.append('### Error:  Validation IDs failed in ' + table_name)

	return (is_has_error == False)

# Save Auto-gen source
def SaveAutoGenSource(filename, code_string):
	with codecs.open('./auto-gen/' + filename, 'w', 'utf-8') as f:
		f.write(code_string)
	
# Build table class
def BuildTableClass():
	global g_error_comment

	timestamp = datetime.datetime.now().strftime('%Y.%m.%d, %H:%M:%S')
	is_has_error = False

	for info in g_table_infos:
		table_name = info[0]
		# base_row = info[1]
		base_col = info[2]
		field_names = info[3]
		field_types = info[4]
		index_eof = info[5]
		# index_eot = info[6]

		c_sharp_ext = '.cs'
		c_sharp_code = "\
// TableTool auto-generated Table class: #CLASS_NAME# at #CURRENT_TIME#\n\
// by jazzcake@gmail.com\n\
\n\
using System.Collections.Generic;\n\
using System;\n\
\n\
namespace GameTable {\n\
	public class #CLASS_NAME# : IGameTable {\n\
#MEMBERS#\n\
		// if load table, this dictionary has all records from json file.\n\
		public static Dictionary<#ENUM_TYPE#, #CLASS_NAME#> table_dic = null;\n\
		\n\
		// create instance based on json file.\n\
		public static #CLASS_NAME# Create(Dictionary<string, object> json_chunk)\n\
		{\n\
			return new #CLASS_NAME#()\n\
			{\n\
#CREATE#\n\
			};\n\
		}\n\
		\n\
		#region [Methods for tools]\n\
		public static bool Load()\n\
		{\n\
			if (table_dic == null)\n\
				table_dic = LoadTable<#ENUM_TYPE#, #CLASS_NAME#>(GetFilename(typeof(#CLASS_NAME#)));\n\
\n\
			return (table_dic != null);\n\
		}\n\
\n\
		public static List<string> GetAllKeys()\n\
		{\n\
			var all_keys = new List<string>();\n\
			foreach (var key in table_dic.Keys)\n\
				all_keys.Add(key.ToString());\n\
			return all_keys;\n\
		}\n\
\n\
		public static bool HasKey(string key_string)\n\
		{\n\
			#ENUM_TYPE# value;\n\
			return Enum.TryParse<#ENUM_TYPE#>(key_string, out value);\n\
		}\n\
\n\
		public static #ENUM_TYPE# ConvertKey(string key_string)\n\
		{\n\
			#ENUM_TYPE# value;\n\
			if (Enum.TryParse<#ENUM_TYPE#>(key_string, out value))\n\
				return value;\n\
\n\
			return (#ENUM_TYPE#)int.MinValue;\n\
		}\n\
\n\
		public static #CLASS_NAME# GetValue(string key_string)\n\
		{\n\
			var key = ConvertKey(key_string);\n\
			if (table_dic.ContainsKey(key))\n\
				return table_dic[key];\n\
			return null;\n\
		}\n\
		#endregion\n\
	};\n\
}"

		# make code
		members_string = ""
		create_string = ""
		key_type_string = ""

		for field_index in range(0, index_eof - base_col):
			field_type = field_types[field_index]
			field_name = field_names[field_index]

			if (field_name[0] == '#'):
				continue
				
			if (IsEnumTypeDefine(field_type)):
				key_type_string = ExtractEnumTypeName(field_type)

			if (IsKeywordType(field_type)):
				if (field_type == '!INT'):
					members_string += '		public int ' + field_name + ';\n'
					create_string += '				' + field_name + ' = (int)((long)json_chunk["' + field_name + '"]),\n'
				elif (field_type == '!LONG'):
					members_string += '		public long ' + field_name + ';\n'
					create_string += '				' + field_name + ' = (long)json_chunk["' + field_name + '"],\n'
				elif (field_type == '!FLOAT'):
					members_string += '		public float ' + field_name + ';\n'
					create_string += '				' + field_name + ' = (float)((double)json_chunk["' + field_name + '"]),\n'
				elif (field_type == '!BOOL'):
					members_string += '		public bool ' + field_name + ';\n'
					create_string += '				' + field_name + ' = ((long)json_chunk["' + field_name + '"] != 0),\n'
				elif (field_type == '!STRING' or field_type == '!STRING32'):
					members_string += '		public string ' + field_name + ';\n'
					create_string += '				' + field_name + ' = json_chunk["' + field_name + '"] as string,\n'
				elif (field_type == '!STRING64'):
					members_string += '		public string ' + field_name + ';\n'
					create_string += '				' + field_name + ' = json_chunk["' + field_name + '"] as string,\n'
				elif (field_type == '!STRING128'):
					members_string += '		public string ' + field_name + ';\n'
					create_string += '				' + field_name + ' = json_chunk["' + field_name + '"] as string,\n'
				elif (field_type == '!STRING256'):
					members_string += '		public string ' + field_name + ';\n'
					create_string += '				' + field_name + ' = json_chunk["' + field_name + '"] as string,\n'
				elif (field_type == '!STRING512'):
					members_string += '		public string ' + field_name + ';\n'
					create_string += '				' + field_name + ' = json_chunk["' + field_name + '"] as string,\n'
				elif (field_type == '!STRING1024'):
					members_string += '		public string ' + field_name + ';\n'
					create_string += '				' + field_name + ' = json_chunk["' + field_name + '"] as string,\n'
				elif (field_type == '!STRINGX'):
					members_string += '		public string ' + field_name + ';\n'
					create_string += '				' + field_name + ' = json_chunk["' + field_name + '"] as string,\n'
				else:
					is_has_error = True
					g_error_comment.append("### Error:  Unknown keyword: " + field_type + ' with ' + field_name + ',' + table_name)
			else:
				if (IsEnumTypeDefine(field_type)):
					members_string += '		public {0} {1};\n'.format(ExtractEnumTypeName(field_type), field_name)
					create_string += '				{1} = ({0})((long)json_chunk["{1}"]),\n'.format(ExtractEnumTypeName(field_type), field_name)
				elif (IsEnumType(field_type)):
					members_string += '		public {0} {1};\n'.format(ExtractEnumTypeName(field_type), field_name)
					create_string += '				{1} = ({0})((long)json_chunk["{1}"]),\n'.format(ExtractEnumTypeName(field_type), field_name)
				else:
					is_has_error = True
					g_error_comment.append("### Error:  Unknown enum: " + field_type + ' with ' + field_name + ',' + table_name)

		c_sharp_code = c_sharp_code.replace('#CLASS_NAME#', table_name).replace('#MEMBERS#', members_string).replace('#CREATE#', create_string).replace('#CURRENT_TIME#', timestamp).replace('#ENUM_TYPE#', key_type_string)
		SaveAutoGenSource(table_name + c_sharp_ext, c_sharp_code)

	if is_has_error == True:
		g_error_comment.append("### Error:  Build failed during generating source files.")
		return False

	return True

def SaveContentsAsJson():
	global g_error_comment

	for info in g_table_infos:
		table_name = info[0]
		# base_row = info[1]
		base_col = info[2]
		field_names = info[3]
		field_types = info[4]
		index_eof = info[5]
		# index_eot = info[6]

		is_has_error = False

		# make members
		field_member_list = []

		type_int = 0
		type_long = 1
		type_bool = 2
		type_float = 3
		type_string = 4

		for field_index in range(0, index_eof - base_col):
			field_type = field_types[field_index]
			field_name = field_names[field_index]

			if (field_name[0] == '#'):
				continue

			if (IsKeywordType(field_type)):
				if (field_type == '!INT'):
					field_member_list.append([field_name, field_index, type_int, 4])
				elif (field_type == '!LONG'):
					field_member_list.append([field_name, field_index, type_long, 8])
				elif (field_type == '!FLOAT'):
					field_member_list.append([field_name, field_index, type_float, 4])
				elif (field_type == '!BOOL'):
					field_member_list.append([field_name, field_index, type_bool, 4])
				elif (field_type == '!STRING' or field_type == '!STRING32'):
					field_member_list.append([field_name, field_index, type_string, 32])
				elif (field_type == '!STRING64'):
					field_member_list.append([field_name, field_index, type_string, 64])
				elif (field_type == '!STRING128'):
					field_member_list.append([field_name, field_index, type_string, 128])
				elif (field_type == '!STRING256'):
					field_member_list.append([field_name, field_index, type_string, 256])
				elif (field_type == '!STRING512'):
					field_member_list.append([field_name, field_index, type_string, 512])
				elif (field_type == '!STRING1024'):
					field_member_list.append([field_name, field_index, type_string, 1024])
				elif (field_type == '!STRINGX'):
					field_member_list.append([field_name, field_index, type_string, 4096])
				else:
					is_has_error = True
					g_error_comment.append("### Error:  Unknown type: " + field_type + ' with ' + field_name + ',' + table_name)
			else:
				if (IsEnumTypeDefine(field_type)):
					field_member_list.append([field_name, field_index, type_int, 4])
				elif (IsEnumType(field_type)):
					field_member_list.append([field_name, field_index, type_int, 4])
				else:
					is_has_error = True
					g_error_comment.append("### Error:  Unknown type: " + field_type + ' with ' + field_name + ',' + table_name)

		if is_has_error:
			g_error_comment.append("### Error:  Can't save json: " + table_name)
			continue

		# save contents to json format
		table_content = g_table_contents[table_name]
		filename = table_name + '.json'
		
		with codecs.open('./json-gen/' + filename, 'w', 'utf-8') as f:
		
			f.write('{\n')
			f.write('	"table":"' + table_name + '",\n')
			f.write('	"count":' + str(len(table_content)) + ',\n')
			f.write('	"contents":[\n')

			for row_index, row_content in enumerate(table_content):
				#print(table_name, row_content)

				f.write('		{\n')
				
				for field_index, f_info in enumerate(field_member_list):
					
					fi = f_info[1]
					ft = f_info[2]
					fsize = f_info[3]
					value = row_content[fi]

					if ft == type_int:
						if type(value) == int:
							value = int(value)
						elif type(value) == float:
							value = int(value)
						elif type(value) == str:
							value = 0
						else:
							g_error_comment.append("#WARNING# in table {0} field_name {1} value {2} type {3}".format(table_name, f_info[0], value, type(value)))
							value = 0
					elif ft == type_long:
						if type(value) == int:
							value = int(value)
						elif type(value) == float:
							value = int(value)
						elif type(value) == str:
							value = 0
						else:
							g_error_comment.append("#WARNING# in table {0} field_name {1} value {2} type {3}".format(table_name, f_info[0], value, type(value)))
							value = 0
					elif ft == type_float:
						if type(value) == float:
							value = float(value)
						elif type(value) == str:
							value = 0.0
						else:
							g_error_comment.append("#WARNING# in table {0} field_name {1} value {2} type {3}".format(table_name, f_info[0], value, type(value)))
							value = 0
					elif ft == type_bool:
						if type(value) == int:
							value = value
						elif type(value) == str:
							value = int(value == 'Y' or value == 'Yes' or value == 'YES' or value == 'TRUE' or value == 'True')
						else:
							g_error_comment.append("#WARNING# in table {0} field_name {1} value {2} type {3}".format(table_name, f_info[0], value, type(value)))
							value = 0
					elif ft == type_string:
						if type(value) == str:
							value = value[0:fsize] #.encode('utf-8')
						elif type(value) == float:
							value = str(int(value))
						else:
							g_error_comment.append("#WARNING# in table {0} field_name {1} value {2} type {3}".format(table_name, f_info[0], value, type(value)))
							value = ""

					# format attribute:value
					if type(value) == int or type(value) == float:
						attribute_value = '			"' + f_info[0] + '" : {0}'.format(value)
					else:
						attribute_value = '			"' + f_info[0] + '" : "{0}"'.format(value)

					if field_index < len(field_member_list) - 1:
						attribute_value += ',\n'
					else:
						attribute_value += '\n'
						
					f.write(attribute_value)
				
				if row_index < len(table_content) - 1:
					f.write('		},\n')
				else:
					f.write('		}\n')
			
			# end of for
			f.write('	]\n')
			f.write('}\n')
			
	print("# '{0}' saved successfully.".format(filename))
			
	return True

def LoadEnumDB():
	global g_table_enums
	global g_error_comment
	
	# load enum db
	try:
		with open('enum_db.json', 'r') as json_data:
			g_table_enums = json.load(json_data)
			json_data.close()
	except IOError:
		print("### Error 'enum_db.json' not exist, start from none.")
		g_table_enums = {}

	print("# 'enum_db.json' loaded successfully.")
	print("\n")
	return

def SaveEnumAsJson():
	global g_table_enums
	
	with open('enum_db.json', 'w') as json_file:
		json.dump(g_table_enums, json_file, sort_keys=True, indent=4, separators=(',', ': '))
		
	print("# 'enum_db.json' saved successfully.")
	return

def SaveEnums():
	global g_error_comment

	# save key enum respectively
	for info in g_table_infos:
		table_name = info[0]
		# base_row = info[1]
		base_col = info[2]
		field_names = info[3]
		field_types = info[4]
		index_eof = info[5]
		# index_eot = info[6]

		enum_source = "\
// TableTool auto-generated Table enum: #ENUM_NAME#\n\
// by jazzcake@gmail.com\n\
\n\
namespace GameTable {\n\
	public enum #ENUM_NAME# {\n\
#ENUMS#\
	};\n\
}"
		for field_index in range(0, index_eof - base_col):
			field_type = field_types[field_index]
			field_name = field_names[field_index]

			if (field_name[0] == '#'):
				continue

			if (IsEnumTypeDefine(field_type)):
				# save all rows as name like 'TableEnum_Character'

				# get field_type without '$'
				field_type = ExtractEnumTypeName(field_type)

				table_content = g_table_contents[table_name]
				enums_string = ""
				
				for row_content in table_content:
					enum_value = ExtractEnumNameAndValue(row_content[field_index], 0)
					enum_key = enum_value[0]
					enum_value = GetEnumAsInt(field_type, enum_value[0])
					enums_string += "		{0} = {1},\n".format(enum_key, enum_value)

				filename = 'TableEnum_' + table_name + '.cs'
				print('> Save ', filename)

				with codecs.open('./enum-gen/' + filename, 'w', 'utf-8') as f:
					f.write(enum_source.replace('#ENUM_NAME#', '{0}'.format(field_type)).replace('#ENUMS#', enums_string))

	return


def SaveEnumsForCpp():
	global g_error_comment

	# save key enum respectively
	for info in g_table_infos:
		table_name = info[0]
		# base_row = info[1]
		base_col = info[2]
		field_names = info[3]
		field_types = info[4]
		index_eof = info[5]
		# index_eot = info[6]

		enum_source = "\
// TableTool auto-generated Table enum: #ENUM_NAME#\n\
// by jazzcake@gmail.com\n\
\n\
namespace GameTable {\n\
	enum #ENUM_NAME# {\n\
#ENUMS#\
	};\n\
}"
		for field_index in range(0, index_eof - base_col):
			field_type = field_types[field_index]
			field_name = field_names[field_index]

			if (field_name[0] == '#'):
				continue

			if (IsEnumTypeDefine(field_type)):
				# save all rows as name like 'TableEnum_Character'

				# get field_type without '$'
				field_type = ExtractEnumTypeName(field_type)

				table_content = g_table_contents[table_name]
				enums_string = ""
				
				for row_content in table_content:
					enum_value = ExtractEnumNameAndValue(row_content[field_index], 0)
					enum_key = enum_value[0]
					enum_value = GetEnumAsInt(field_type, enum_value[0])
					enums_string += "		{0} = {1},\n".format(enum_key, enum_value)

				filename = 'TableEnum_' + table_name + '.hpp'
				print('> Save ', filename)

				with codecs.open('./hpp-gen/' + filename, 'w', 'utf-8') as f:
					f.write(enum_source.replace('#ENUM_NAME#', '{0}'.format(field_type)).replace('#ENUMS#', enums_string))

	return


#----------------------------------------------------------------------------------------------
# __main__
if __name__ == '__main__':
	print('=========================================================')
	
	# load key-value enum db table first
	LoadEnumDB()
	
	# collect all excel file in ./res
	current_dir = './res' # os.getcwd()
	all_files = []

	for file in os.listdir(current_dir):
		if file.endswith(".xlsx"):
			all_files.append(current_dir + '/' + file)

	for file in all_files:
		ParseSingleExcelFile(file)

	if g_error_comment == None or len(g_error_comment) == 0:
		# save enum before replacing id with value
		SaveEnums()
		SaveEnumsForCpp()

		# validate & replace IDs with key int value
		if ReplaceIDs() == True:
			# build class for each table
			if BuildTableClass() == True:
				# save all contents as json format
				SaveContentsAsJson()
				SaveEnumAsJson()

	# show result of build
	error_code = 0

	if g_error_comment != None and len(g_error_comment) > 0:
		print("##########################################################")
		print('> Build failed. You have', len(g_error_comment), 'error.')

		for e in g_error_comment:
			print('<color=red>', e, '</color>')

		print("##########################################################")
		error_code = 1

	else:
		print('=========================================================')
		print('> Build complete successfully.')

	sys.exit(error_code)
